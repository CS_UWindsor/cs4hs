nums = [1,2,3,4]
fruit = ["Apples", "Peaches", "Pears", "Bananas"]

# some more complex examples
l6 = [(i,f) for i in nums for f in fruit if f[0] == "P" if i%2 == 1]

# converting l6 into a dictionary
d1 = {}
for k,v in l6:
    d1.setdefault(k, []).append(v)
    