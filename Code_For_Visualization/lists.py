# an emply list
l1 = []

# a simple list comprehension
l2 = [x for x in range(10)]

# filtering the values assigned
l3 = [x for x in range(10) if x%2==0]

nums = [1,2,3,4]
fruit = ["Apples", "Peaches", "Pears", "Bananas"]

# some more complex examples
l4 = [(i,f) for i in nums for f in fruit]
l5 = [(i,f) for i in nums for f in fruit if f[0] == "P"]
l6 = [(i,f) for i in nums for f in fruit if f[0] == "P" if i%2 == 1]
l7 = [i for i in zip(nums,fruit) if i[0]%2==0]
