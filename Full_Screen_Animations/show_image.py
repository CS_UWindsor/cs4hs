"""
Load and show one image on the HDMI device...
"""
 
import sys
import time
import pygame
from fbscreen import fbscreen

screen = fbscreen().screen

image1 = pygame.image.load('/home/pi/data/nature.jpg').convert()


screen.blit(image1, (0,0))
pygame.display.flip()
    
time.sleep(10) # sleep for ten seconds
    