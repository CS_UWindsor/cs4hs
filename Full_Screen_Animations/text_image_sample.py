#
# This sample program will demonstrate drawing to the HDMI screen, loops and timers
#

import os
import pygame
import sys
import time
import random

from fbscreen import fbscreen
from datetime import datetime

# Create an instance a special class to draw directly on the framebuffer (defined in fbscreen.py)

print "Initializing Screen..."

screen = fbscreen().screen

# Get width and height of this drawing area and store them for future use
size = width, height = (pygame.display.Info().current_w, pygame.display.Info().current_h)

# Define fonts for the initial time display, and the small banner

font = pygame.font.SysFont('sans-serif', 160, True)
label_font = pygame.font.SysFont('monospace', 20, True)

current_time = datetime.now().strftime('%Y-%m-%d %H:%M')

text1 = font.render(current_time, True, (255, 255, 255))
# text_rect is the containing rectangle
text_rect = text1.get_rect(center=(width / 2, height / 2))
# textsurface is the image object that contains the text
textsurface = pygame.surface.Surface((width,height))
textsurface.blit(text1, ((width-text_rect.width)/2,(height-text_rect.height)/2)) # copy text to rectangle
# The first item in the imagelist list is the text banner (starting time)
imagelist = [textsurface]
# The rest are the images from the files. Define the filenames here
imagefiles = ['/home/pi/data/nature.jpg','/home/pi/data/skyline.jpg','/home/pi/data/landscape.jpg','/home/pi/data/sunset.jpg'];

# load the image files, add them to the list, and scale if necessary

for imagefile in imagefiles:
    print "Loading image file " + imagefile
    image = pygame.image.load(imagefile)
    imagelist.append( pygame.transform.smoothscale(image, (width,height)).convert())

previous_image = None

print "Program will loop until ..."

# Setup Initial Variables

alpha_val = 0        # This cycles between 0 and 225
image_index = 0      # This ranges in the size of the imagelist

done = False         # A flag to exit the loop

# Start the loop...

while not done:
    # Check for events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
            pygame.quit()
            sys.exit()
 
    # Modify the main variables.
    # The alpha (transparency value) should be increased, if it reaches the limit, start at zero
    alpha_val = alpha_val + 3
    if alpha_val >= 225:
        # The limit has been reached, the image is at full brightness
        alpha_val = 0
        # Make the fade-out image the curent image
        previous_image = imagelist[image_index]
        # Select the next image to start the fade effect
        image_index = image_index + 1
        # If we have hit the list, start over again
        if image_index >= len(imagelist):
            image_index = 0

    if previous_image == None:
        # fade in text only - this is only done once at the start of the sequence
        imagelist[0].set_alpha(alpha_val)
        screen.blit(imagelist[0],(0,0)) 
    else:      
        # cross fade two images
        imagelist[image_index].set_alpha(alpha_val)
        previous_image.set_alpha(225-alpha_val)
        screen.blit(imagelist[image_index], (0,0))
        screen.blit(previous_image, (0,0))
    # Display a diagnostic message
    msg = datetime.now().strftime('%l:%M:%S %p %Z')
    msg_label = label_font.render(str(msg), True, (255, 255, 255))
    msg_rect = msg_label.get_rect(top=5, right=width - 5)
    msg_surface = pygame.surface.Surface((msg_rect.width,msg_rect.height))
    msg_surface.blit(msg_label, (0,0))
    screen.blit(msg_surface, msg_rect)
    msg_surface.set_alpha(255)
    
    pygame.display.flip()

